//
//  CategoriesViewController.swift
//  TMDT
//
//  Created by DUYMAC on 7/24/16.
//  Copyright © 2016 Hung Duong. All rights reserved.
//

import UIKit

struct Category {
    var title:String
    var subtitle:String
    var imgURL:String
}

class CategoriesViewController: UIViewController {
    
    
    @IBOutlet weak var tableViewCategories: UITableView!
    
    
    var categoriesArr:[Category] = [
        Category(title: "Coffee", subtitle: "Freshly brewed coffee", imgURL:"coffee" ),
        Category(title: "Breakfast", subtitle: "Perfectly baked & served warm", imgURL:"breakfast" ),
        Category(title: "Munchies", subtitle: "Perfectly baked & served warm", imgURL:"munchies" ),
        Category(title: "Sandwiches", subtitle: "Fresh, healthy and tasty ", imgURL:"sandwiches" ),
        Category(title: "Specialty Drinks", subtitle: "Special drinks for every taste ", imgURL:"drinks" )
    ]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // estimatedRowHeight : ước lượng height
        tableViewCategories.estimatedRowHeight  = 100
        tableViewCategories.rowHeight           = UITableViewAutomaticDimension
        
        tableViewCategories.dataSource          = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension CategoriesViewController: UITableViewDataSource {
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell            = tableViewCategories.dequeueReusableCellWithIdentifier("catCell", forIndexPath: indexPath)
        let cat             = categoriesArr[indexPath.row]
        let bgImgView       = cell.viewWithTag(100) as! UIImageView
        bgImgView.image     = UIImage(named: cat.imgURL)
        
        let titleLabel      = cell.viewWithTag(200) as! UILabel
        titleLabel.text     = cat.title
        
        let subtitleLabel   = cell.viewWithTag(300) as! UILabel
        subtitleLabel.text  = cat.subtitle
        

        
        return cell
    }
}