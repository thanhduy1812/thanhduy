//
//  LoginViewController.swift
//  TMDT
//
//  Created by DUYMAC on 7/24/16.
//  Copyright © 2016 Hung Duong. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameTF.layer.borderColor = mainColor.CGColor
        usernameTF.attributedPlaceholder = NSAttributedString(string: "  Username", attributes: [NSForegroundColorAttributeName: mainColor])
        usernameTF.layer.borderWidth = 1.0
        
        passwordTF.layer.borderColor = mainColor.CGColor
        passwordTF.attributedPlaceholder = NSAttributedString(string: "  Password", attributes: [NSForegroundColorAttributeName: mainColor])
        passwordTF.layer.borderWidth = 1.0

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
